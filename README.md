# General

All discussions about the organization of the teams within the Defend stage.

Discussions related to Product development must happen in [gitlab](https://gitlab.com/gitlab-org/gitlab) issue trackers.

## Outgoing MRs

As a part of [our Engineering Metrics](https://about.gitlab.com/handbook/engineering/management/engineering-metrics/#list-of-metrics),
we aim at measuring Merge Request rates both internally and externally. The list
below covers our outgoing MRs per milestone to community open source projects.
Additions to this list should be applied [using both `group` and `stage` labels
to properly track throughput](https://about.gitlab.com/handbook/engineering/management/throughput/#stage-and-group-labels-in-throughput).

### %13.3

- cilium: [Use the global Azure key](https://github.com/cilium/cilium/pull/12683)

### %13.1

 - cilium: [Fix FilterLine test matchers and related specs](https://github.com/cilium/cilium/pull/11794)
 - cilium: [Add audit action to the policy verdict log](https://github.com/cilium/cilium/pull/11843)

### %12.10

 - cilium: [Handle audit mode in cilium endpoint list and kubectl get cep](https://github.com/cilium/cilium/pull/11011)

### %12.9

 - hubble: [Add options to the flow metrics](https://github.com/cilium/hubble/pull/117) 
 - cilium: [Improve pod restarts on GKE](https://github.com/cilium/cilium/pull/10377)

### %12.8

 - cilium: [Implement policy audit mode for the daemon](https://github.com/cilium/cilium/pull/9970)

### %12.7

 - ingress-nginx: [Update Modsecurity-nginx to latest (v1.0.1)](https://github.com/kubernetes/ingress-nginx/pull/4842)
 - cilium: [Improve nodeinit uninstalls by reverting nodeinit changes](https://github.com/cilium/cilium/pull/9757)
 - cilium: [Add cilium-monitor sidecar container for agent pods](https://github.com/cilium/cilium/pull/9815)
 - cilium: [Add helm charts packaging steps to the release script](https://github.com/cilium/cilium/pull/9772)
 - cilium: [Use helm repository in docs](https://github.com/cilium/cilium/pull/9783)
 - cilium: [Fix helm subchart versions](https://github.com/cilium/cilium/pull/9826)
